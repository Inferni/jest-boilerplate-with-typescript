/** 
points(1, 1) ➞ 5 
 
points(7, 5) ➞ 29 
 
points(38, 8) ➞ 100 
 
points(0, 1) ➞ 3 
 
points(0, 0) ➞ 0
 */

import { points } from ".";

/**
 * Arg 1: describes test
 * Arg 2: Function - code for test
 */
it("should add 1 two and 1 three to equal 5", () => {
    // at least 1 assertion
    expect(points(1,1)).toBe(5);
});


 it("should add 38 two and 8 three to equal 100", () => {
    // at least 1 assertion
    expect(points(38,8)).toBe(100);
});

 it("should add 0 two and 0 three to equal 0", () => {
    // at least 1 assertion
    expect(points(0,0)).toBe(0);
});

it("should throw for negative points", () => {
    // at least 1 assertion
    expect(()=>points(-1,0)).toThrow("Negative two points");
});