import { charCount } from "."

it("should return 1 if 1 character matches", () => {
    // at least 1 assertion
    expect(charCount("a", "edabit")).toBe(1);
});

it("should return 1 if 1 character matches, case sensitive", () => {
    // at least 1 assertion
    expect(charCount("c", "Chamber of secrets")).toBe(1);
});

it("should return 0 if 0 characters match, case sensitive", () => {
    // at least 1 assertion
    expect(charCount("B", "boxes are fun")).toBe(0);
});

it("should return 4 if 4 characters match", () => {
    // at least 1 assertion
    expect(charCount("b", "big fat bubble")).toBe(4);
});

it("should return 0 if 0 characters match", () => {
    // at least 1 assertion
    expect(charCount("e", "javascript is good")).toBe(0);
});

it("should return 2 if 2 characters match, symbol test", () => {
    // at least 1 assertion
    expect(charCount("!", "!easy!")).toBe(2);
});
