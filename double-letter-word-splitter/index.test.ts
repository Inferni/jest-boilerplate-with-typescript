
import {splitOnDoubleLetter} from "."

it("should return ['let', 'ter'] if given Letter", () => {
    // at least 1 assertion
    expect(splitOnDoubleLetter("Letter")).toEqual(['let', 'ter'] );
});

it("should return ['real', 'ly'] if given Really", () => {
    // at least 1 assertion
    expect(splitOnDoubleLetter("Really")).toEqual(['real', 'ly'] );
});

it("should return ['hap', 'py'] if given Happy", () => {
    // at least 1 assertion
    expect(splitOnDoubleLetter("Happy")).toEqual(['hap', 'py'] );
});

it("should return ['mis','sis','sip','pi']  if given 'Mississippi'", () => {
    // at least 1 assertion
    expect(splitOnDoubleLetter("Mississippi")).toEqual(['mis','sis','sip','pi'] );
});

it("should return [] if given Easy", () => {
    // at least 1 assertion
    expect(splitOnDoubleLetter("Easy")).toEqual([] );
});
