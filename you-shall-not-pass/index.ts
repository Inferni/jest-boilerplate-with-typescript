export function passwordStrengthCheck(password) {
    let passwordStrength = 0;
    let lowercase = false;
    let uppercase = false;
    let special = false;
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    let length = false;
    let digit = false;

    // Password to short
    if (password.length < 6 || password.indexOf(' ')>=0) {
        console.log(password + " to short or has white spaces");
        
        return "Invalid";
    }

    if (password.length >= 8) {        
        passwordStrength++;
        length = true;
    }

    if( specialChars.test(password))
    {
        passwordStrength++;      
        special = true;
    }

    for (let i = 0; i < password.length; i++) {
        const character = password[i];
        
        if (character == character.toLowerCase() && !lowercase && isNaN(character * 1)) {   
            passwordStrength++;
            lowercase = true;
        }

        if (character == character.toUpperCase() && !uppercase && isNaN(character * 1)) {
            passwordStrength++;
            uppercase = true;
        }

        if (!isNaN(character * 1) && !digit) {
            passwordStrength++;
            digit = true;
        }
    }

    if(!lowercase)
    {
        console.log(password + " does not have lowercase character");
        
    }

    if(!uppercase)
    {
        console.log(password + " does not have uppercase character");
        
    }

    if(!special)
    {
        console.log(password + " does not have special character");
        
    }

    if(!digit)
    {
        console.log(password + " does not contain a number");
        
    }
    switch (passwordStrength) {
        case 1:
            return "Weak";
        case 2:
            return "Weak";
        case 3:
            return "Moderate";
        case 4:
            return "Moderate";
        case 5:
            return "Strong";
        default:
            return "Invalid"
    }

}